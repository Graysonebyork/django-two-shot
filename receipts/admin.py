from django.contrib import admin
from receipts.models import Account, Receipt, ExpenseCategory

@admin.register(Account)
class AccountAdmin(admin.ModelAdmin):
    account_display = ["name", "number", "owner"]

@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    receipt_display = ["vendor", "total", "tax", "date", "purchaser", "category", "account"]

@admin.register(ExpenseCategory)
class ExpenseCategoryAdmin(admin.ModelAdmin):
    expensecategory_display = ["name", "owner"]
