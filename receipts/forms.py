from django import forms
from django.forms import ModelForm
from django.conf import settings
from receipts.models import Receipt

class CreateReceiptForm(ModelForm):
    class Meta:
        model = Receipt
        fields = [
            "vendor",
            "total",
            "tax",
            "date",
            "category",
            "account",
            ]
