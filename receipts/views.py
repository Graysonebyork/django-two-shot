from django.shortcuts import render, redirect
from receipts.models import Receipt, ExpenseCategory, Account
from receipts.forms import CreateReceiptForm
from django.contrib.auth.decorators import login_required

@login_required
def show_receipt(request):
    receipt_list = Receipt.objects.filter(purchaser=request.user)
    context = {
        "receipt_list_object": receipt_list,
    }

    return render(request, "receipts/receipt_list.html", context)

def create_receipt(request):
    if request.method == "POST":
        form = CreateReceiptForm(request.POST)
        if form.is_valid():
            receipt = form.save(False)
            receipt.purchaser = request.user
            receipt.save()
            return redirect("home")
    else:
        form = CreateReceiptForm()

    context = {
        "form": form
    }
    return render(request, "receipts/create_receipt.html", context)
