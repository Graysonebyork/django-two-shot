from django.db import models
from django.conf import settings
#from django.contrib.auth.models import User

# Create your models here.

class Account(models.Model):
    name = models.CharField(null=True, max_length=100)
    number = models.CharField(null=True, max_length=20)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="accounts",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.name

class ExpenseCategory(models.Model):
    name = models.CharField(null=True, max_length=50)
    owner = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="categories",
        on_delete=models.CASCADE,
        null=True
    )

    def __str__(self):
        return self.name


class Receipt(models.Model):
    vendor = models.CharField(null=True, max_length=200)
    total = models.DecimalField(null=True, max_digits=10, decimal_places=3)
    tax = models.DecimalField(null=True, max_digits=10, decimal_places=3)
    date = models.DateTimeField(null=True)
    purchaser = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True
    )
    category = models.ForeignKey(
        ExpenseCategory,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
    account = models.ForeignKey(
        Account,
        related_name="receipts",
        on_delete=models.CASCADE,
        null=True,
    )
